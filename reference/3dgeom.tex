\chapter{3D Geometry}

\begin{Definition}[point] \index{point}
A \emph{point} $P(x, y, z) \in \real$ describes a position in space.
\end{Definition}

\begin{Definition}[vector] \index{vector}
A \emph{vector} $\vec v(x, y, z) \in \real$ describes a displacement in space.
\end{Definition}

\begin{Definition}[addition] \index{addition!vector}\index{vector!addition}
The addition of two points/vectors is equal to
\[
  (x,y,z) + (x',y',z') = (x+x',y+y',z+z')
\]
Addition is possible on the following operand types:
\begin{center}
  \begin{tabular}{ccc}
    \textbf{left operand} & \textbf{right operand} & \textbf{result} \\
    \toprule
    point & vector & point \\
    vector & point & point \\
    vector & vector & vector \\
  \end{tabular}
\end{center}
\end{Definition}

\begin{extra}
  \begin{example}
  Adding a point $P(1,1,0)$ with a vector $\vec v(2,1,0)$ corresponds to moving
  $P$ in a direction and over a distance specified by $\vec v$.
  \begin{center}
    \begin{tikzpicture}
      \draw[thin,gray] (0,0) grid (4,4);
      \coordinate (p) at (1,1);
      \coordinate (q) at (3,2);

      \draw[-latex,thick] (0,0) -- (4,0);
      \draw[-latex,thick] (0,0) -- (0,4);

      \point[/point/label={$P$},/point/position={(p)}]
      \draw[vector] (p) -- (q) node[midway,sloped,above] {$\vec v$};
      \point[/point/label={$P+\vec v$},/point/position=(q)]
    \end{tikzpicture}
  \end{center}
  \end{example}
\end{extra}

\begin{extra}
  \begin{example}
  Adding two vectors $\vec u$ and $\vec v$ corresponds to combining
  both displacements into one.
  \begin{center}
    \begin{tikzpicture}
      \draw[thin,gray] (0,0) grid (4,4);

      \draw[-latex,thick] (0,0) -- (4,0);
      \draw[-latex,thick] (0,0) -- (0,4);

      \draw[vector] (1,1) -- (2,1) node[midway,above] {$\vec u$};
      \draw[vector] (2,1) -- (2,3) node[midway,right] {$\vec v$};
      \draw[vector] (1,1) -- (2,3) node[midway,sloped,above] {$\vec u+\vec v$};
    \end{tikzpicture}
  \end{center}
  \end{example}
\end{extra}

\begin{Definition}[subtraction] \index{subtraction!vector}\index{vector!subtraction}
The subtraction of two points/vectors is equal to
\[
  (x,y,z) - (x',y',z') = (x-x',y-y',z-z')
\]
Subtraction is possible on the following operand types:
\begin{center}
  \begin{tabular}{ccc}
    \textbf{left operand} & \textbf{right operand} & \textbf{result} \\
    \toprule
    point & point & vector \\
    point & vector & point \\
    vector & vector & vector \\
  \end{tabular}
\end{center}
\end{Definition}

\begin{Definition}[scalar multiplication] \index{multiplication}\index{scalar multiplication}
The scalar multiplication of a scalar $f \in \real$ and a vector $\vec v(x,y,z)$ is equal to
\[
  f \cdot (x, y, z) = (x, y, z) \cdot f = (f \cdot x, f \cdot y, f \cdot z)
\]
\end{Definition}

\begin{extra}
  \begin{example}
  Scalar multiplication corresponds to stretching or shrinking the vector: its orientation
  remains the change but its length is modified.
  \begin{center}
    \begin{tikzpicture}
      \draw[vector] (0,0) -- (20:2) node[anchor=south] { $\vec v$ };
      \draw[vector,yshift=-1mm] (0,0) -- (20:4) node[anchor=south] { $2 \vec v$ };
      \draw[vector,yshift=-2mm] (0,0) -- (20:6) node[anchor=south] { $3 \vec v$ };
      \draw[vector,yshift=-3mm] (0,0) -- (20:-2) node[anchor=south] { $-\vec v$ };
      \draw[vector,yshift=-4mm] (0,0) -- (20:-4) node[anchor=south] { $-2\vec v$ };
      \draw[vector,yshift=-5mm] (0,0) -- (20:-6) node[anchor=south] { $-3\vec v$ };
    \end{tikzpicture}
  \end{center}
  \end{example}
\end{extra}

\begin{Definition}[length of a vector] \index{vector!length}\index{length}
The \emph{length} of a vector $\vec v(x, y, z)$ equals
\[
  \norm{\vec{v}} = \sqrt{x^2+y^2+z^2}
\]
\end{Definition}

\begin{Definition}[dot product] \label{def:dot-product} \index{vector!dot product}\index{dot product}\index{multiplication!dot product}
The \emph{dot product} of two vectors $\vec{u}(x, y, z)$ and $\vec{v}(x', y', z')$ is equal to
\[
  \vec u \cdot \vec v = x \cdot x' + y \cdot y' + z \cdot z' = \norm{\vec u} \cdot \norm{\vec v} \cdot \cos \theta
\]
where $\theta$ is the angle between the two vectors.
\begin{center}
  \begin{tikzpicture}
    \draw[vector] (0,0) -- (30:2);
    \draw[vector] (0,0) -- (60:3);
    \draw (30:1) arc [start angle=30,end angle=60,radius=1cm] node[midway,anchor=south west] {$\theta$};
  \end{tikzpicture}
\end{center}
\end{Definition}

\begin{extra}
  \begin{example}
  We can use the dot product to determine the angle between vectors:
  \[
    \vec{u} \cdot \vec{v} = \norm{\vec u} \cdot \norm{\vec v} \cdot \cos\theta
    \qquad\Rightarrow\qquad
    \cos\theta = \frac{\vec u \cdot \vec v}{\norm{\vec u} \cdot \norm{\vec v}}
  \]
  We apply this on the following vectors:
  \begin{center}
    \begin{tikzpicture}
      \draw[vector] (0,0) -- (2,1) node[anchor=south] {$\vec u = (2,1,0)$};
      \draw[vector] (0,0) -- (1,2) node[anchor=south] {$\vec v = (1,2,0)$};
    \end{tikzpicture}
  \end{center}
  \[
    \cos\theta = \frac{\vec u \cdot \vec v}{\norm{\vec u} \cdot \norm{\vec v}}
               = \frac{2 + 2 + 0}{\sqrt{5} \sqrt{5}}
               = \frac{4}{5}
    \qquad\Rightarrow\qquad
    \theta = \arccos \frac45 = 36.87\degrees
  \]
  \end{example}
\end{extra}

\begin{theorem}
Two nonzero length vectors $\vec u$ and $\vec u$ are perpendicular iff $\vec u \cdot \vec v = 0$.
\end{theorem}
\begin{extra}
  \begin{proof}
  According to \cref{def:dot-product}
  \[
    \vec u \cdot \vec v = \norm{\vec u} \cdot \norm{\vec v} \cdot \cos \theta
  \]
  A product is zero if one of its factors is zero. Given that both vectors have nonzero lengths ($\norm{\vec u} \neq = 0$ and $\norm{\vec v} \neq 0$),
  the only remaining possibility is that $\cos \theta = 0$. This is the case when $\theta = 90\degrees + k \cdot 180\degrees$ with $k \in \Z$, all
  of which correspond to perpendicularity.
  \end{proof}
\end{extra}

\begin{theorem}
For any vector $\vec v$,
\[
  \norm{\vec v}^2 = \vec v \cdot \vec v
\]
\end{theorem}
\begin{extra}
  \begin{proof}
  From \cref{def:dot-product},
  \[
    \vec v \cdot \vec v = \norm{\vec v} \cdot \norm{\vec v} \cdot \cos \theta
  \]
  A vector always makes an angle of $0 \degrees$ with itself. Since $\cos 0\degrees = 1$, we get
  \[
    \vec v \cdot \vec v = \norm{\vec v} \cdot \norm{\vec v} = \norm{\vec v}^2
  \]
  \end{proof}
\end{extra}

\begin{theorem} \label{thm:dot-product-projection}
The dot product $\vec u \cdot \vec v$ is equal to $\norm{\vec u} \cdot \norm{\vec v_\+p}$,
where $\vec v_\+p$ is the projection of $\vec v$ on $\vec u$.
\begin{center}
  \begin{tikzpicture}[rotate=20]
    \draw[dotted] let \p1=(30:4) in (\p1) -- (\x1,0) -- (0,0);
    \draw[vector,yshift=-2mm] let \p1=(30:4) in (0,0) -- (\x1,0) node[anchor=north] {$\vec v_\+p$};
    \draw[vector] (0,0) -- (2,0) node[anchor=south] {$\vec u$};
    \draw[vector] (0,0) -- (30:4) node[anchor=south] {$\vec v$};
  \end{tikzpicture}
\end{center}
\end{theorem}

\begin{Definition}[cross product] \index{cross product}\index{vector!cross product}\index{multiplication!cross product}
The \emph{cross product} of two vectors $\vec u(x, y, z)$ and $\vec{v}(x', y', z')$ is equal to
\[
  \vec u \times \vec v =
  \left|
    \begin{array}{ccc}
      \vec{X} & \vec{Y} & \vec{Z} \\
      x & y & z \\
      x' & y' & z'
    \end{array}
  \right|
  =
 \left( \begin{array}{r@{\;}c@{\;}l}
          y z' - y' z \\
          x' z - x z' \\
          xy' - x'y \\
        \end{array} \right)
\]
\end{Definition}

\begin{theorem}
Given two vectors $\vec{u}(x, y, z)$ and $\vec{v}(x', y', z')$, their cross product $\vec u \times \vec v$ is perpendicular on both of them.
\end{theorem}
\begin{extra}
  \begin{proof}
  We need to prove that
  \[
    (\vec u \times \vec v) \cdot \vec u = 0 \qquad (\vec u \times \vec v) \cdot \vec v = 0
  \]
  We prove the left equation:
  \[
    \begin{array}{rcl}
      (y z' - y' z, x' z - x z', xy' - x'y) \cdot (x, y, z) & = & x \cdot (y z'-y' z) + y \cdot (x' z-x z') + z \cdot (xy' - x'y) \\ 
                                                            & = & x y z' - x y' z + x' y z - x y z' + x y' z - x' y z \\
                                                            & = & 0
    \end{array}
  \]
  Idem for the right equation.
  \end{proof}
\end{extra}

\begin{Definition}[ray] \index{ray}
A \emph{ray} consists of all points on a straight line.
A ray is uniquely defined by a starting point $P$ and a (nonzero) direction vector $\vec d$.
The set of all points can then be written as
\[
  \{ \; P + t \cdot \vec d \;|\; t \in \real \; \}
\]
\begin{center}
  \begin{tikzpicture}
    \draw[-latex,thick] (0,0) -- (5,0);
    \draw[-latex,thick] (0,0) -- (0,4);

    \point[/point/label=P,/point/position={(1,1)}]
    \draw[vector] (1,1) -- +(30:1) node[midway,sloped,above] {$\vec d$};
    \draw (1,1) -- +(210:2.5);
    \draw (1,1) -- ++(30:4.5);

    \coordinate (q) at ($ (1,1) + (30:1) $);
    \foreach \t in {-2,-1,0,1,2,3,4} {
      \coordinate (p) at ($ (1,1) ! \t ! (q) $);
      \draw[fill=black] (p) circle (.025);
      \node[font=\tiny,anchor=north west,inner sep=1pt] at (p) {t=\t};
    }
  \end{tikzpicture}
\end{center}
\end{Definition}

\begin{Definition}[plane] \index{plane}
A \emph{plane} is unambiguously defined by a point $P$ and a (nonzero) normal vector $\vec n$.
The set of all points on a plane can be written
\[
  \{ \; Q \;|\; (Q-P) \cdot \vec n = 0 \; \}
\]
i.e.\ the set of all points for which the vector $\overrightarrow{PQ} = Q-P$ is perpendicular on $\vec n$.
\end{Definition}

\begin{theorem}[ray-plane intersection] \label{thm:ray-plane}
The intersection of a ray with starting point $P$ and direction vector $\vec d$
and a plane with point $Q$ and normal vector $\vec n$ can be found using the following formula:
\[
  P + \frac{(Q-P) \cdot \vec n}{\vec d \cdot \vec n} \cdot \vec d
\]
If $\vec d \cdot \vec n = 0$, there is no intersection.
\end{theorem}
\begin{extra}
  \begin{proof}
  In order to find the intersection, we need to solve the following equation for $t$:
  \[
    ((P + t \cdot \vec d) - Q) \cdot \vec n = 0
  \]
  Expanding gives us
  \[
    (P - Q) \cdot \vec n + t \cdot (\vec d \cdot \vec n) = 0
  \]
  Solving for $t$
  \[
    t = \frac{(Q-P) \cdot \vec n}{\vec d \cdot \vec n}
  \]
  Plugging in $t$ in $P + t \cdot \vec d$ yields the desired result.
  \end{proof}
\end{extra}

\begin{extra}
  \begin{example}
  We consider a situation where we can easily determine the intersection ourselves.
  Take the XY plane, i.e.\ point $Q = (0,0,0)$ and $\vec n = (0,0,1)$.
  Take a ray parallel to the Z-axis shooting straight down towards the plane, i.e.\ starting point $P = (5,3,7)$
  and $\vec d = (0,0,-1)$. It should hit the plane at $(5,3,0)$.

  Applying the formula from \cref{thm:ray-plane} gives us
  \[
    \begin{array}{rl}
      & \displaystyle (5,3,7) + \frac{((0,0,0) - (5,3,7)) \cdot (0,0,1)}{(0,0,-1)\cdot(0,0,1)} \cdot (0,0,-1) \\ \\
      = & \displaystyle (5,3,7) + \frac{(-5,-3,-7) \cdot (0,0,1)}{(0,0,-1)\cdot(0,0,1)} \cdot (0,0,-1) \\ \\
      = & \displaystyle (5,3,7) + \frac{-7}{-1} \cdot (0,0,-1) \\ \\
      = & \displaystyle (5,3,7) + (0,0,-7) \\ \\
      = & \displaystyle (5,3,0)
    \end{array}
  \]
  \end{example}
\end{extra}

\begin{extra}
  \begin{example}
  We can generalise the previous example in the following ways:
  \begin{itemize}
    \item It does not matter which point $Q$ of the plane we take. Any point $(\alpha, \beta, 0)$ with $\alpha,\beta \in \real$ will do.
    \item The normal vector $\vec n$ can be of any length, so we generalise it to $\vec n = (0,0,\gamma)$ with $\gamma \in \real_0$.
    \item The height of the ray's starting position $P$ does not matter: we can take $P = (5,3,\delta)$ with $\delta \in \real$.
    \item The length of the direction vector does not matter, so we take $d = (0,0,\epsilon)$ with $\epsilon \in \real_0$.
  \end{itemize}
  Applying the formula from \cref{thm:ray-plane} should still give us the exact same result:
  \[
    \begin{array}{rl}
      & \displaystyle (5,3,\delta) + \frac{((\alpha,\beta,0) - (5,3,\delta)) \cdot (0,0,\gamma)}{(0,0,\epsilon)\cdot(0,0,\gamma)} \cdot (0,0,\epsilon) \\ \\
      = & \displaystyle (5,3,\delta) + \frac{-\delta \cdot \gamma}{\epsilon \cdot \gamma} \cdot (0,0,\epsilon) \\ \\
      = & \displaystyle (5,3,\delta) + \frac{-\delta}{\epsilon} \cdot (0,0,\epsilon) \\ \\
      = & \displaystyle (5,3,\delta) + (0,0,-\delta) \\ \\
      = & \displaystyle (5,3,0)
    \end{array}
  \]
  \end{example}
\end{extra}

\begin{Definition}[sphere] \index{sphere}
A \emph{sphere} is the set of all points at an equal distance from a centre point.
A sphere is uniquely defined by its center $C$ and its radius $r$. The set
of all points on a sphere can be written
\[
  \{ \; P \;|\; (P-C) \cdot (P-C) = r^2 \; \}
\]
\end{Definition}

\begin{theorem}[ray-sphere intersection]
Given a ray with starting position $P$ and direction vector $\vec d$.
Given a sphere with centre $O(0,0,0)$ and radius 1.
To find the intersections between these two, perform the following steps:
\begin{enumerate}
  \item Compute $a = \vec d \cdot \vec d$
  \item Compute $b = 2 \cdot (P - O) \cdot \vec d$
  \item Compute $c = (P - O) \cdot (P - O) - r ^ 2$
  \item Compute $D = b^2 - 4 \cdot a \cdot c$
  \item If $D < 0$, there are no intersections.
  \item If $D = 0$, the intersection is located at
        \[
          P + \frac{-b}{2 \cdot a} \cdot \vec d
        \]
  \item If $D > 0$, there are two intersections at
        \[
          P + \frac{-b \pm \sqrt D}{2 \cdot a} \cdot \vec d
        \]
\end{enumerate}
\end{theorem}
\begin{extra}
  \begin{proof}
  All points on the rays can be written $P + t \cdot \vec d$.
  All points $Q$ on the sphere must satisfy $(Q - O) \cdot (Q - O) = 1$.
  Putting these two things together gives
  \[
    (P + t \cdot \vec d - O) \cdot (P + t \cdot \vec d - O) = 1
  \]
  Expanding
  \[
    (P - O) \cdot (P - O) + \vec d \cdot \vec d \cdot t^2 + 2 \cdot (P - O) \cdot \vec d \cdot t = 1
  \]
  We rearrange the terms
  \[
    (\vec d \cdot \vec d) \cdot t^2 + (2 \cdot (P - O) \cdot \vec d) \cdot t + (P - O) \cdot (P - O) - 1 = 0
  \]
  We perform the following substitutions:
  \[
    \begin{array}{rcl}
      a & = & \vec d \cdot \vec d \\
      b & = & 2 \cdot (P - O) \cdot \vec d \\
      c & = & (P - O) \cdot (P - O) - 1 \\
    \end{array}
  \]
  yielding
  \[
    a \cdot t^2 + b \cdot t + c = 0
  \]
  This is a quadratic equation. The discriminant is
  \[
    D = b^2 - 4 \cdot a \cdot c
  \]
  \begin{itemize}
    \item If $D < 0$, the ray misses the sphere and there are no intersections.
    \item If $D = 0$, the ray grazes the sphere and there is one intersection at $t = \frac{-b}{2 \cdot a}$.
    \item If $D > 0$, the ray enters and exists the sphere in two different intersection points at
          \[
            t = \frac{-b \pm \sqrt D}{2 \cdot a}
          \]
  \end{itemize}
  \end{proof}
\end{extra}

\begin{extra}
  \begin{example}
  Consider the ray with starting point $P = (0,0,5)$ and direction vector $\vec d = (0,0,-1)$. We expect
  the ray to hit the sphere in places, namely $(0,0,1)$ and $(0,0,-1)$.
  We perform the necessary computations:
  \begin{itemize}
    \item $a = \vec d \cdot \vec d = 1$.
    \item $b = 2 \cdot (P - O) \cdot \vec d = 2 \cdot (0,0,5) \cdot (0,0,-1) = -10$.
    \item $c = (P-O) \cdot (P-O) - r^2 = (0,0,5) \cdot (0,0,5) - 1^2 = 24$.
    \item $D = b^2 - 4 \cdot a \cdot c = 100 - 96 = 4$.
  \end{itemize}
  Since $D > 0$, there are two intersections $I_1$ and $I_2$.
  \[
    \begin{array}{r@{\;}c@{\;}l}
      I_1 & = & \displaystyle P + \frac{-b-\sqrt D}{2 \cdot a} \cdot \vec d = (0,0,5) + \frac{10 - 2}{2} \cdot (0,0,-1) = (0,0,5) + (0,0,-4) = (0,0,1) \\ \\
      I_2 & = & \displaystyle P + \frac{-b+\sqrt D}{2 \cdot a} \cdot \vec d = (0,0,5) + \frac{10 + 2}{2} \cdot (0,0,-1) = (0,0,5) + (0,0,-6) = (0,0,-1) \\ \\
    \end{array}
  \]
  \end{example}
\end{extra}

\begin{extra}
  \begin{exercise}
  Find the intersections of a ray and a cylinder.
  \end{exercise}
\end{extra}

\begin{extra}
  \begin{exercise}
  Find the intersections of a ray and a cone.
  \end{exercise}
\end{extra}

\begin{theorem}[reflection] \label{thm:reflection} \index{reflection}
The \emph{reflection} of a vector $\vec v$ by a unit vector $\vec n$ can be computed as follows:
\[
  \vec v - 2 \cdot (\vec v \cdot \vec n) \cdot \vec n
\]
\end{theorem}
\begin{extra}
  \begin{proof}
  We make a quick sketch of the situation:
  \begin{center}
    \begin{tikzpicture}
      \draw[ultra thick] (-2,0) -- (2,0);
      \draw[vector] (0,0) -- (0,1) node[anchor=south] { $\vec n$ };
      \draw[vector] (120:2) -- (0,0) node[at start,anchor=south] { $\vec v$ };
      \draw[vector] (0,0) -- (60:2) node[at end,anchor=south] { $\vec r$ };
    \end{tikzpicture}
  \end{center}
  We decompose $v$ in a horizontal and vertical component:
  \begin{center}
    \begin{tikzpicture}
      \draw[ultra thick] (-2,0) -- (2,0);
      \draw[vector] (0,0) -- (0,1) node[anchor=south] { $\vec n$ };
      \draw[vector] (120:2) -- (0,0) node[at start,anchor=south] { $\vec v$ };
      \draw[vector] let \p1=(120:2) in (\p1) -- (\x1,0) node[midway,anchor=east] { $\vec v_\+y$ };
      \draw[vector] let \p1=(120:2) in (\p1) -- (0,\y1) node[midway,anchor=south] { $\vec v_\+x$ };
      \draw[vector] (0,0) -- (60:2) node[at end,anchor=south] { $\vec r$ };
    \end{tikzpicture}
  \end{center}
  We can see that $\vec r = \vec v_\+x - \vec v_\+y$. If we can find a way to determine
  these $\vec v_\+x$ and $\vec v_\+y$, we're set. We cannot just take the X- and Y-coordinate,
  as it is possible that the reflecting surface is not perfectly horizontal:
  \begin{center}
    \begin{tikzpicture}[rotate=30]
      \draw[ultra thick] (-2,0) -- (2,0);
      \draw[vector] (0,0) -- (0,1) node[anchor=south] { $\vec n$ };
      \draw[vector] (120:2) -- (0,0) node[at start,anchor=south] { $\vec v$ };
      \draw[vector] (0,0) -- (60:2) node[at end,anchor=south] { $\vec r$ };
    \end{tikzpicture}
  \end{center}
  We can find $\vec v_\+y$ by projecting $\vec v$ onto $\vec n$ (see \cref{thm:dot-product-projection}):
  \[
    \vec v_\+y = (\vec v \cdot \vec n) \cdot \vec n
  \]
  We can then easily find $\vec v_\+x$ as follows:
  \[
    \vec v_\+x = \vec v - \vec v_\+y
  \]
  Substituting yields us the final result:
  \[
    \vec r = \vec v_\+x - \vec v_\+y
           = \vec v - \vec v_\+y - \vec v_\+y
           = \vec v - 2 \cdot \vec v_\+y
           = \vec v - 2 \cdot (\vec v \cdot \vec n) \cdot \vec n
  \]
  \end{proof}
\end{extra}

\begin{extra}
  \begin{example}
  \begin{center}
    \begin{tikzpicture}
      \draw[vector] (0,0) -- (1,0) node[at end,anchor=west] { $\vec n = (1,0,0)$ };
      \draw[vector] (2,1) -- (0,0) node[at start,anchor=south] { $\vec v = (-2,-1,0)$ };
      \draw[vector] (0,0) -- (2,-1) node[at end,anchor=north] { $\vec r$ };
    \end{tikzpicture}
  \end{center}
  We compute $\vec r$'s coordinates using \cref{thm:reflection}.
  \[
    \begin{array}{r@{\;}c@{\;}l}
      \vec v - 2 \cdot (\vec v \cdot \vec n) \cdot \vec n
      & = & (-2,-1,0) - 2 \cdot ((-2,-1,0) \cdot (1,0,0)) \cdot (1,0,0) \\
      & = & (-2,-1,0) - 2 \cdot -2 \cdot (1,0,0) \\
      & = & (-2,-1,0) + (4,0,0) \\
      & = & (2,-1,0)
    \end{array}
  \]
  \end{example}
\end{extra}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: "reference"
%%% End:
