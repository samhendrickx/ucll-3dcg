\chapter{Matrix Transformations}
\begin{Definition}[matrix] \index{matrix}
An $n \times m$ \emph{matrix} is a rectangular array of values with $N$ rows and $M$ columns.
\[
  \left[
  \begin{array}{cccc}
    a_{1,1} & a_{1,2} & \dots & a_{1,M} \\
    a_{2,1} & a_{2,2} & \dots & a_{2,M} \\
    \vdots & \vdots & \ddots & \vdots \\
    a_{N,1} & a_{N,2} & \dots & a_{N,M} \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{Definition}[transpose] \index{transpose}\index{matrix!transpose}
The \emph{transpose} of a $N \times M$ matrix $a_{i,j}$ is the $M \times N$ matrix $b_{i,j}$ whose
rows are the columns of the original matrix.
\[
  b_{i,j} = a_{j,i}
\]
Notation: $\transpose{A}$.
\end{Definition}

\begin{extra}
  \begin{example}
  \[
    \transpose{\left[
      \begin{array}{ccc}
        1 & 2 & 3 \\
        4 & 5 & 6 \\
      \end{array}
    \right]} =
    \left[
      \begin{array}{cc}
        1 & 4 \\
        2 & 5 \\
        3 & 6 \\
      \end{array}
    \right]
  \]
  \end{example}
\end{extra}

\begin{Definition}[matrix multiplication] \index{matrix!multiplication}\index{multiplication!matrix}
The multiplication of an $N \times M$ matrix $a_{i,j}$ with an $M \times K$ matrix $b_{i,j}$ is defined as
the $N \times K$ matrix $c_{i,j}$ where
\[
  c_{i,j} = \sum_{x=1 \dots M} a_{i,x} b_{x,j}
\]
In other words, the element on the $i$-th row and $j$-th column
is equal to the dot product of the $i$-th row of the left matrix
and the $j$-th row of the right matrix.
\end{Definition}

\begin{extra}
  \begin{example}
  \[
    \left[
    \begin{array}{ccc}
      1 & 0 & 2 \\
      -1 & 3 & 1 \\
      0 & 1 & 2 \\
    \end{array}
    \right]
    \cdot
    \left[
    \begin{array}{ccc}
      0 & 1 & -2 \\
      -1 & 3 & 2 \\
      2 & 0 & 5
    \end{array}
    \right]
    =
    \left[
    \begin{array}{ccc}
      4 & 1 & 8 \\
      -1 & 8 & 13 \\
      3 & 3 & 12
    \end{array}
    \right]
  \]
  \end{example}
\end{extra}

\begin{Definition}[zero matrix] \index{zero matrix}\index{matrix!zero}
The \emph{zero matrix} of size $N \times M$ is the matrix where all elements are equal to zero:
$a_{1 \dots N,1 \dots M} = 0$.
\end{Definition}

\begin{Definition}[square matrix] \index{square matrix}\index{matrix!square}
A \emph{square matrix} is a matrix with an equal number of rows and columns.
\end{Definition}

\begin{Definition}[main diagonal] \index{main diagonal}\index{matrix!main diagonal}
The \emph{main diagonal} of a matrix consists of the elements $a_{i,i}$.
\[
  \left[
  \begin{array}{cccc}
    \mathbf{a_{1,1}} & a_{1,2} & a_{1,3} & \dots \\
    a_{2,1} & \mathbf{a_{2,2}} & a_{2,3} & \dots \\
    a_{3,1} & a_{3,2} & \mathbf{a_{3,3}} & \dots \\
    \vdots & \vdots & \vdots & \ddots \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{Definition}[identity matrix] \index{identity matrix}\index{matrix!identity}
The \emph{identity matrix} of size $N$ is the square zero matrix of size $N \times N$
whose main diagonal contains ones. Notation: $I$.
\end{Definition}

\begin{extra}
  \begin{example}
  The identity matrix of size 4 is
  \[
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & 0 \\
      0 & 1 & 0 & 0 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]
  \]
  \end{example}
\end{extra}

\begin{theorem} \label{thm:identity-multiplication}
Multiplying a matrix with the identity matrix results in the original matrix.
\[
  A \cdot I = I \cdot A = A
\]
\end{theorem}

\begin{Definition}[inverse of a matrix] \label{def:inverse-matrix} \index{inverse}\index{matrix!inverse}
The inverse of a square matrix $A$ is the square matrix $\inverse{A}$ for which
\[
  A \cdot \inverse{A} = \inverse{A} \cdot A = I
\]
\end{Definition}

\begin{theorem}
\[
  \inverse{(A \cdot B)} = \inverse{B} \cdot \inverse{A}
\]
\end{theorem}
\begin{extra}
  \begin{proof}
  We know from \cref{def:inverse-matrix} that if
  $\inverse{B} \cdot \inverse{A}$ is to be the inverse of $A \cdot B$,
  it needs to satisfy
  \[
    (A \cdot B) \cdot (\inverse{B} \cdot \inverse{A}) = I \qquad \inverse{B} \cdot \inverse{A}) \cdot (A \cdot B) = I
  \]
  We focus on the left equality. Using associativity and \cref{thm:identity-multiplication}, we get
  \[
    (A \cdot B) \cdot (\inverse B \cdot \inverse A) =
    A \cdot (B \cdot \inverse B) \cdot \inverse A =
    A \cdot I \cdot \inverse A =
    A \cdot \inverse A =
    I
  \]
  We can prove the right equality analogously.
  \end{proof}
\end{extra}

\begin{Definition}[translation] \index{translation}
\emph{Translation} by a vector $\vec v(x,y,z)$ is represented
by the matrix
\[
  \left[
  \begin{array}{cccc}
    1 & 0 & 0 & x \\
    0 & 1 & 0 & y \\
    0 & 0 & 1 & z \\
    0 & 0 & 0 & 1 \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{Definition}[applying a transformation]
To apply a transformation represented by a matrix $M$ to
a point $P(x,y,z)$, one needs to perform the following multiplication:
\[
  M \cdot \left[
            \begin{array}{c}
              x \\
              y \\
              z \\
              1 \\
            \end{array}
          \right]
\]
This yields a new matrix of the form
$\left[ \begin{array}{c} x' \\ y' \\ z' \\ 1 \end{array} \right]$.
The transformed point's coordinates are then $P'(x',y',z')$.
\end{Definition}

\begin{extra}
  \begin{example}
  Applying a translation described by $\vec v(2,-1,3)$ to a point $P(-1,3,0)$ gives
  \[
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & 2 \\
      0 & 1 & 0 & -1 \\
      0 & 0 & 1 & 3 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]
    \cdot
    \left[
    \begin{array}{c}
      -1 \\
      3 \\
      0 \\
      1 \\
    \end{array}
    \right]
    =
    \left[
    \begin{array}{c}
      1 \\
      2 \\
      3 \\
      1 \\
    \end{array}
    \right]
  \]
  This agrees with the result we get from $P + \vec v = (1,2,3)$.
  \end{example}
\end{extra}

\begin{extra}
  \begin{example}
  Applying a translation described by $\vec v(2,-1,0)$ to a unit circle gives
  \[
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & 2 \\
      0 & 1 & 0 & -1 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]
    \cdot
    \left[
    \begin{array}{c}
      \cos \theta \\
      \sin \theta \\
      0 \\
      1 \\
    \end{array}
    \right]
    =
    \left[
    \begin{array}{c}
      \cos(\theta) + 2 \\
      \sin(\theta) - 1 \\
      0 \\
      1 \\
    \end{array}
    \right]
  \]
  \begin{center}
    \begin{tikzpicture}
      \draw[-latex,thick] (-2,0) -- (4,0);
      \draw[-latex,thick] (0,-3) -- (0,2);

      \draw (0,0) circle (1);
      \draw[dashed] (2,-1) circle (1);
    \end{tikzpicture}
  \end{center}
  \end{example}
\end{extra}

\begin{Definition}[scaling]
\emph{Scaling} by factors $s_x$, $s_y$, $s_z$ is represented
by the matrix
\[
  \left[
  \begin{array}{cccc}
    s_x & 0 & 0 & 0 \\
    0 & s_y & 0 & 0 \\
    0 & 0 & s_z & 0 \\
    0 & 0 & 0 & 1 \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{extra}
  \begin{example}
  Scaling a unit circle by $(3, \frac12,1)$ gives
  \[
    \left[
    \begin{array}{cccc}
      3 & 0 & 0 & 0 \\
      0 & \frac12 & 0 & 0 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]
    \cdot
    \left[
    \begin{array}{c}
      \cos \theta \\
      \sin \theta \\
      0 \\
      1 \\
    \end{array}
    \right]
    =
    \left[
    \begin{array}{c}
      3\cos(\theta)\\
      \frac12\sin(\theta) \\
      0 \\
      1 \\
    \end{array}
    \right]
  \]
  \begin{center}
    \begin{tikzpicture}
      \draw[-latex,thick] (-4,0) -- (4,0);
      \draw[-latex,thick] (0,-2) -- (0,2);

      \draw (0,0) circle (1);
      \draw[dashed] (0,0) circle [x radius=3cm,y radius=0.5cm];
    \end{tikzpicture}
  \end{center}
  \end{example}
\end{extra}

\begin{Definition}[rotation around X-axis] \index{rotation!X-axis}
Rotation around the X-axis by an angle $\theta$ is represented by
\[
  \left[
  \begin{array}{cccc}
    1 & 0 & 0 & 0 \\
    0 & \cos\theta & -\sin\theta & 0 \\
    0 & \sin\theta & \cos\theta & 0 \\
    0 & 0 & 0 & 1 \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{Definition}[rotation around Y-axis] \index{rotation!Y-axis}
Rotation around the Y-axis by an angle $\theta$ is represented by
\[
  \left[
  \begin{array}{cccc}
    \cos\theta & 0 & \sin\theta & 0 \\
    0 & 1 & 0 & 0 \\
    -\sin\theta & 0 & \cos\theta & 0 \\
    0 & 0 & 0 & 1 \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{Definition}[rotation around Z-axis] \index{rotation!Z-axis}
Rotation around the Z-axis by an angle $\theta$ is represented by
\[
  \left[
  \begin{array}{cccc}
    \cos\theta & -\sin\theta & 0 & 0 \\
    \sin\theta & \cos\theta & 0 & 0 \\
    0 & 0 & 1 & 0 \\
    0 & 0 & 0 & 1 \\
  \end{array}
  \right]
\]
\end{Definition}

\begin{extra}
  \begin{example}
  The inverse of a transformation matrix corresponds to the inverse transformation.
  Consider the translation by $(2,5,-1)$, which has transformation matrix
  \[
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & 2 \\
      0 & 1 & 0 & 5 \\
      0 & 0 & 1 & -1 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]  
  \]
  The inverse translation would be described by $(-2,-5,1)$ or
  \[
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & -2 \\
      0 & 1 & 0 & -5 \\
      0 & 0 & 1 & 1 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]  
  \]
  Multiplying these two matrices gets us
  \[
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & 2 \\
      0 & 1 & 0 & 5 \\
      0 & 0 & 1 & -1 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]
    \cdot
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & -2 \\
      0 & 1 & 0 & -5 \\
      0 & 0 & 1 & 1 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]  
    =
    \left[
    \begin{array}{cccc}
      1 & 0 & 0 & 0 \\
      0 & 1 & 0 & 0 \\
      0 & 0 & 1 & 0 \\
      0 & 0 & 0 & 1 \\
    \end{array}
    \right]  
  \]
  \end{example}
\end{extra}


%%% Local Variables:
%%% mode: latex
%%% TeX-master: "reference"
%%% End:
